<%-- 
    Document   : Home
    Created on : Jan 7, 2024, 9:04:10 PM
    Author     : Admin
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="entity.Post" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.Vector" %>
<%@page import="entity.Product" %>
<%@page import="entity.Category" %>
<%@page import="entity.Slider" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="//cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
        <script src="//cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
        <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" rel="stylesheet">
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
        <script src="js/script.js"></script>
        <!------ Include the above in your HEAD tag ---------->
        <style>
            .pagination {
                display: flex;
                margin-left: 20px;
            }
            .pagination a {
                margin-left: 5px;
                color: black;
                font-size: 12px;
                float: left;
                padding: 8px 16px;
                text-decoration: none;
            }
            .pagination a.active {
                background-color: #4CAF50;
                color: white;
            }
            .pagination a:hover:not(.active) {
                background-color: chocolate;
        </style>
    </head>
    <body>
        <%  
            int i = 0;
            Post lastPost = (Post)request.getAttribute("lPost");
            Vector<Slider> listS = (Vector<Slider>)request.getAttribute("listS");
            Vector<Product> listP =(Vector<Product>)request.getAttribute("listP");
            Vector<Category> listC =(Vector<Category>)request.getAttribute("listC");
            Product last = (Product)request.getAttribute("p");
             int tag = (int)request.getAttribute("tag");
            Object value = request.getAttribute("txtS");
            String txtS;
                if(value==null){
                txtS="";
            }else txtS = (String)request.getAttribute("txtS");
        %>
        <nav class="navbar navbar-expand-md navbar-dark bg-dark">
            <div class="container-fluid">
                <a class="navbar-brand" href="home">Shop</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarsExampleDefault">
                    <ul class="navbar-nav m-auto">
                        <%-- <%if(acc != null){%> 

                    <%if(acc.getIsSale() == 1){%> 
                    <li class="nav-item">
                        <a class="nav-link" href="managerOrder">Manager Order</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="managerPro">Manager Product</a>    <%-- sản phẩm người đó bán --%>     
                        </li>

                        <%--                  <%}%>
                                        <%if(acc.getIsAdmin() == 1){%>  
                                        <li class="nav-item">
                                            <a class="nav-link" href="managerAcc">Manager Account</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="adDashboard">Dashboard</a> 
                                        </li>

                    <%}%>
                    <%if(acc.getIsMKT() == 1){%>  
                    <li class="nav-item">
                        <a class="nav-link" href="postList">Post list</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="mktDashboard">Dashboard</a> 
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="productList">All Product</a> <%-- tất cả sản phẩm --%> 
                        <%--          </li>
                                 <li class="nav-item">
                                     <a class="nav-link" href="customerList">Customer List</a>
                                 </li>
                                 <li class="nav-item">
                                     <a class="nav-link" href="feedList">Feed List</a>
                                 </li>

                    <%}%>
                    <%if(acc.getIsSaleManager() == 1){%> 
                    <li class="nav-item">
                        <a class="nav-link" href="manager">Manager All Product</a> <%-- tất cả sản phẩm --%> 
                        <%--         </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="smdashboard">Dashboard</a> 
                                </li>
                                <%}%>

                    <li class="nav-item">
                        <a class="nav-link" href="#">Welcome: <%=acc.getUser()%></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="blog">Blog</a>
                    </li> 
                    <li class="nav-item">
                        <a class="nav-link" href="logout">Logout</a>
                    </li> 
                    <li class="nav-item">
                        <a class="nav-link" href="feedBack">Feed Back</a>
                    </li>
                    <%}%>       
                    <%if(acc == null){%> --%>  
                        <li class="nav-item">
                            <a class="nav-link" href="blog">Blog</a>
                        </li> 
                        <li class="nav-item">
                            <a class="nav-link" href="login">Login</a>
                        </li>                                       
                        <%-- <%}%>--%> 
                    </ul>

                    <ul class="navbar-nav ml-auto"> 
                        <form action="search" method="post" class="form-inline my-2 my-lg-0 ml-auto">
                            <div class="input-group input-group-sm">
                                <input value ="<%=txtS%>"  name="txt" type="text" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm" placeholder="Search...">
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-secondary btn-number">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>
                            </div>
                            <a class="btn btn-success btn-sm ml-3" href="show">
                                <i class="fa fa-shopping-cart"></i> Cart
                                <span class="badge badge-light"> <%--<%=proInCart%>--%></span>
                            </a>                          
                        </form>
                        <%--    <% if (acc != null) { %>  --%>
                        <li class="nav-item">
                            <a class="icon-link d-flex align-items-center my-2 ml-5" href="userprofile"> 
                                <span>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-person-vcard" viewBox="0 0 16 16">
                                    <path d="M5 8a2 2 0 1 0 0-4 2 2 0 0 0 0 4m4-2.5a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1h-4a.5.5 0 0 1-.5-.5M9 8a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1h-4A.5.5 0 0 1 9 8m1 2.5a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 0 1h-3a.5.5 0 0 1-.5-.5"/>
                                    <path d="M2 2a2 2 0 0 0-2 2v8a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V4a2 2 0 0 0-2-2zM1 4a1 1 0 0 1 1-1h12a1 1 0 0 1 1 1v8a1 1 0 0 1-1 1H8.96q.04-.245.04-.5C9 10.567 7.21 9 5 9c-2.086 0-3.8 1.398-3.984 3.181A1 1 0 0 1 1 12z"/>
                                    </svg>
                                </span>
                                <span class="ml-2">Use Profile</span>
                            </a>
                        </li>   
                        <%--     <% } %>    --%>
                    </ul>

                </div>
            </div>
        </nav>    
        <div id="carouselExample" class="carousel slide">
            <div class="carousel-inner">
                
                <% for (Slider slider : listS) { %>
                <div class="carousel-item <%=i == 0 ? "active" : "" %>">
                    <a href="<%= slider.getBacklink() %>">
                        <img src="<%=slider.getsImage()%>" class="d-block mx-auto" alt="<%=slider.getsTitle()%>">
                    </a>
                </div>
                <% i++; %>
                <% } %>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExample" data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExample" data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button>

        
            <span class="carousel-indicators">                
                   <% for ( i = 0; i < listS.size(); i++) { %>               
                <li data-bs-target="#carouselExample" data-bs-slide-to="<%= i %>" class="<%= i == 0 ? "active" : "" %>"></li>
                    <%}%>            
            </span>

        </div>

        <!--end of menu-->
        <div class="container">
            <div class="row">
                <div class="col">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="home">Home</a></li>
                            <li class="breadcrumb-item"><a href="#">Category</a></li>
                            <li class="breadcrumb-item active" aria-current="#">Sub-category</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
        <div class="container"> <%-- category --%> 
            <div class="row">
                <div class="col-sm-3">
                    <div class="card bg-light mb-2">
                        <div class="card-header bg-dark text-white text-uppercase"><i class="fa fa-list"></i> Categories</div>
                        <ul class="list-group category_block">
                            <%for (Category cat : listC){%>
                            <li class="list-group-item text-white <%=cat.getCategoryId()==tag?"active":""%>"><a class="text-decoration-none" href="category?categoryId=<%=cat.getCategoryId()%>"><%=cat.getCategoryName()%></a></li>
                                <%}%>
                        </ul>
                    </div>   
                    <div class="card bg-light mb-3"> <%-- quảng cáo --%> 
                        <div class="card-header bg-dark text-white text-uppercase">Last product</div>
                        <div class="card-body">
                            <img class="img-fluid" src="${p.image}" />
                            <a class="nav-link" href="detail?pid=${p.productID}"> <h5 class="card-title">${p.title}</h5></a>
                            <p class="card-text">${p.briefInfo}</p>
                            <del class="bloc_left_price">${p.price}</del>
                            <h3 class="bloc_left_price">${p.salePrice}</h3>
                        </div>
                    </div>
                    <%-- post mới nhất --%>
                    <div class="card bg-light mb-3"> 
                        <div class="card-header bg-dark text-white text-uppercase">NEW POST</div>
                        <div class="card-body">                            
                            <img class="img-fluid" src="${lPost.postThumbnail}" />
                           <a class="nav-link" href="postDetail"> <h5 class="card-title">${lPost.pTitle}</h5></a>
                            <p class="card-text">${lPost.postAuthor}</p>
                            <p class="card-text">${lPost.postBriefInfo}</p>                           
                        </div>
                    </div>
                </div>


                <div class="col-sm-9"> <%-- show sản phẩm --%> 
                    <div class="row text-center">
                        <% for (Product pro : listP){%>
                        <div class="col-12 col-md-6 col-lg-3">
                            <div class="card">
                                <img class="card-img-top" src="<%=pro.getImage()%>" alt="Card image cap">
                                <div class="card-body">
                                    <h4 class="card-title show_txt"><a class="text-decoration-none" href="detail?pid=<%=pro.getProductID()%>" title="View Product"><%=pro.getTitle()%></a></h4>
                                    <p class="card-text show_txt"><%=pro.getBriefInfo()%></p>
                                    <div class="row">                              
                                        <div class="col">
                                             <del class="card-text text-muted me-2"><%=pro.getPrice()%>Đ</del> <h3 class="card-text"><%=pro.getSalePrice()%>Đ</h3> 
                                        </div>
                                        <div class="col">
                                            <a href="show?id=<%=pro.getProductID()%>&name=<%=pro.getTitle()%>&image=<%=pro.getImage()%>&price=<%=pro.getPrice()%>&salePrice=<%=pro.getSalePrice()%>" class="btn btn-dark btn-block">Add to cart</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <%}%>
                    </div>
                </div>

            </div>
        </div>
                        <div class="container-fluid text-center d-flex align-items-center justify-content-center">
                        <c:set var="page" value="${requestScope.page}" />
                        
                        <div class="pagination">
                            <h2>Page: </h2>
                            <c:forEach begin="${1}" end="${requestScope.num}" var="i">
                                <a href="home?page=${i}" class="${i==page?"active":""}" ><h5>${i}</h5> </a>
                            </c:forEach>
                        </div>
                    </div>
        <!-- đáy -->
        <footer class="text-light">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3 col-lg-4 col-xl-3">
                        <h5>About</h5>
                        <hr class="bg-light mb-2 mt-0 d-inline-block mx-auto w-25">
                        <p class="mb-0">
                            liên quan đến sản phẩm
                        </p>
                    </div>

                    <div class="col-md-4 col-lg-3 col-xl-3">
                        <h5>Contact</h5>
                        <hr class="bg-light mb-2 mt-0 d-inline-block mx-auto w-25">
                        <ul class="list-unstyled">
                            <li><i class="fa fa-home mr-2"></i> My Shop</li>
                            <li><i class="fa fa-envelope mr-2"></i> duongvdhe173014@gmail.com</li>
                            <li><i class="fa fa-phone mr-2"></i> + 33 12 14 15 16</li>
                            <li><i class="fa fa-print mr-2"></i> + 33 12 14 15 16</li>
                        </ul>
                    </div>             
                </div>
            </div>
        </footer>
    </body>
</html>
